<?php get_header() ?>

<div class="col-sm-11">
    <div class="card mt-7">

        <div class="card-body">
            <div class="coteG">
                <h2 class="card-title"><?php the_title(); ?></h2>
                <p class="card-text"><?php the_field('taille'); ?> toises</p>
                <p class="card-text"><?php the_field('description'); ?></p>
            </div>
            <div class="coteD col-sm-7">
                <?php the_post_thumbnail('post-thumbnail', ['class' => 'card-img-top', 'alt' => 'image'] ) ?>
                <?php
            $featured_posts = get_field('regime_alimentaire');
            if( $featured_posts ): ?>
                <p>Régime alimentaire :</p>
                <ul>
                    <?php foreach( $featured_posts as $post ): 

        // Setup this post for WP functions (variable must be named $post).
        setup_postdata($post); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </li>
                    <?php endforeach; ?>
                </ul>
                <?php 
    // Reset the global post object so that the rest of the page works correctly.
    wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>



        </div>
    </div>
</div>

<?php get_footer() ?>